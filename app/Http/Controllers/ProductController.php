<?php

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\Category;
use App\Models\Image;
use App\Models\Price;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use League\Csv\Reader;
use App\Services\ImportService;
use App\Services\CsvToJsonService;


/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{

    public function show(Request $request) : ProductResource
    {

        return new ProductResource(Product::with('categories','prices','images')->where('identifier',$request->identifier)->first());

    }

    public function process(Request $request, ImportService $importService)
    {
        $data = $request->json()->all();

        $message = $importService->import($data);

        return response()->json([
            'success' => $message
        ]);

    }

    public function csv(ImportService $importService, CsvToJsonService $csvToJsonService)
    {

        $data = $csvToJsonService->process();

        $message = $importService->import($data);

        return response()->json([
            'success' => $message
        ]);

    }


}
