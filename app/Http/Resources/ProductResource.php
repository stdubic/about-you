<?php

namespace App\Http\Resources;


use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;


class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $categories = $this->categories;

        foreach ($categories as $catgory){
            $formatCategories[] = $catgory['name'];
        }

        $images = $this->images;

        foreach ($images as $image){
            $formatImages[] = $image['name'];
        }

        return [

            'name' => $this->name,
            'identifier' => $this->identifier,
            'description' => $this->description,
            'categories' => $formatCategories,
            'prices' => $this->prices,
            'images' => $formatImages,

        ];
    }
}
