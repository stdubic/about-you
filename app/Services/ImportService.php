<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Image;
use App\Models\Price;
use App\Models\Product;

class ImportService
{
    public function import($data)
    {

        foreach ($data as $row) {
            $product = Product::firstOrCreate([
                'name' => $row['name'],
                'identifier' => $row['identifier'],
                'description' => $row['description']
            ]);

            $categories = [];
            foreach ($row['categories'] as $category) {

                $categories[] = Category::create([
                    'name' => $category,
                ])->id;

            }
            $categories = Category::find($categories);
            $product->categories()->sync($categories);

            $images = [];
            foreach ($row['images'] as $image) {


                $images[] = Image::create([
                    'name' => $image,
                ])->id;

            }
            $images = Image::find($images);
            $product->images()->sync($images);

            $prices = [];
            foreach ($row['prices'] as $price) {
                $prices[] = Price::create([
                    'price' => $price['price'],
                    'valid_from' => date('Y-m-d h:i:s', strtotime($price['validFrom'])),
                    'valid_to' => date('Y-m-d h:i:s', strtotime($price['validTo']))

                ])->id;
            }

            $prices = Price::find($prices);
            $product->prices()->sync($prices);

        }
        return response()->json([
            'success' => 'true'
        ]);
    }
}
