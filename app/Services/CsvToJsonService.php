<?php

namespace App\Services;

use League\Csv\Reader;

class CsvToJsonService
{
    protected $path;

    public function __construct()
    {
        $this->path = '../storage/app/sfpt-input-test-data.csv';
    }

    public function getFile()
    {
        return Reader::createFromPath($this->path);
    }

    public function process()
    {

        $inputCsv = $this->getFile();

        $inputCsv->setDelimiter(';');

        $res = json_encode($inputCsv, JSON_PRETTY_PRINT|JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);

        $res = json_decode($res, true);

        foreach ($res as $key) {
            unset($res[0]);
        }

        $format = [];
        $i = 0;
        foreach ($res as $array)
        {

            foreach ($array as $arr)
            {
                $format[$i]['name'] = $array[0];
                $format[$i]['identifier'] = $array[1];
                $format[$i]['description'] = $array[2];

                $categories = $array[3];

                $categories = explode(',', $categories);

                $a = 0;
                foreach ($categories as $category)
                {
                    $format[$i]['categories'][$a] = $category;
                    $a++;
                }

                $prices = $array[4];

                $a = 0;

                $prices = explode(',', $prices);

                foreach ($prices as $item)
                {
                    $it = explode('|', $item);
                    foreach ($it as $t=>$value)
                    {
                        $format[$i]['prices'][$a]['price'] = $it[0];
                        $format[$i]['prices'][$a]['validFrom'] = $it[1];
                        $format[$i]['prices'][$a]['validTo'] = $it[2];

                    }

                    $a++;
                }

                $images = $array[5];

                $images = explode(',', $images);

                $a = 0;
                foreach ($images as $image)
                {
                    $format[$i]['images'][$a] = $image;
                    $a++;
                }
            }
            $i++;
        }

        return $format;

    }
}
