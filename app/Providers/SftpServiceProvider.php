<?php

namespace App\Providers;

use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Sftp\SftpAdapter;

class SftpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('sftp', function($app, $config) {
            unset($config['driver']);

            foreach($config as $key => $value) {
                if(!strlen($value)) {
                    unset($config[$key]);
                }
            }

            $adapter = new SftpAdapter($config);

            return new Filesystem($adapter);
        });
    }
}
