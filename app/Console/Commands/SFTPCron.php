<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class SFTPCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sftp:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to SFTP server every 2 hours to fetch the a file which has the products information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Storage::disk('local')->put('sfpt-input-test-data.csv', Storage::disk('sftp')->get('sfpt-input-test-data.csv'));
        Log::info("SFTP file saved");

    }
}
