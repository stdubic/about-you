<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ExportCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the data to a certain API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token = 'some token';
        $uri = 'some url';
        $data = 'some json data';


        $client = new Client();
        $response = $client->post($uri, [
            'headers' => [
                'authorization' => 'Bearer'. $token,
                'content-type'  => 'application/json'
            ],
            \GuzzleHttp\RequestOptions::JSON => [
                $data
            ],
        ]);
    }
}
