<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Product;
use App\Models\Price;
use App\Models\Image;
use App\Http\Resources\ProductResource;

use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;

use Illuminate\Support\Facades\Storage;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/product',  'App\Http\Controllers\ProductController@show');
Route::post('/testSFTP', function (Request $request) {


    dd(Storage::disk('local')
        ->put('readme.txt', Storage::disk('sftp')
            ->get('readme.txt')));

});

Route::post('/test','App\Http\Controllers\ProductController@csv' );
Route::get('/data', function (Request $request) {
    return Product::with('categories','prices','images')->get();
});
Route::post('/data',  'App\Http\Controllers\ProductController@process');


