## Description
You need to design an application which receive the date from two data sources (API and SFTP Server), the application should be able to receive the data and store it in a database and periodically export the data to another external system via API.
The application should have an API which can be used to request certain product details.

## Requirements
* The application has an API which clients can use to send us the product data (see **Inputs** section for details about how the client will send us the products details).
* The application can connect to SFTP server every 2 hours to fetch the a file which has the products information (see **Inputs** section for more details about how the csv file is structured).
* The application should have an API which is used to fetch the product details by product identifier (see **Outputs** for more details)
* The application should periodically (every 4 hours) export the data to a certain API (see **Outputs** for more details)
* Consider writing the needed Unit and Feature tests

## Technologies
* PHP >= 7.3 
* Laravel >= 7.0
* Mysql >= 8.0
* Docker
* Redis (Optional)

## Hints
* the same product can be sent again and again, we need to avoid processing the product data if we don't have any change.
## Inputs
#### CSV
* **name**
the product name
* **identifier**
A unique identifier for the product
* **description**
the product description
* **categories**
a comma separated categories, the product can have multiple categories and doesn't necessary means nested ex:
`Gedeckter Tisch 1300` and `Schale/ Platte/ Tablett/ Etagere,Schale/ Platte/ Tablett/ Etagere` are considered as two categories
* **prices**
a comma separated prices, the price entry is divided by `|` ex: `9.96|2021-02-01 00:00:00|2021-02-28 23:59:59` and is mapped as the following:
    * first: is the price value ex: `9.96`
    * second: valid from date ex: `2021-02-01 00:00:00`
    * third: valid to date ex: `2021-02-28 23:59:59`
* **images**
a comma separated images for the product

#### Json
the attributes will be same as the csv file but instead will be a json format
```
   {
        "name": "Sofa Samt ca. B191xT78xH78cm, hellgrau",
        "identifier": "AAC0055773",
        "description": "Sofa Samt ca. B191xT78xH78cm, hellgrau",
        "categories": [
            "Möbel Indoor 2600",
            "Polstermöbel",
            "Polstermöbel"
        ],
        "prices": [
            {
                "price": 8.93,
                "validFrom": "2020-11-01T00:00:00.000000Z",
                "validTo": "2020-11-30T23:59:59.999999Z"
            },
            {
                "price": 8.91,
                "validFrom": "2020-12-01T00:00:00.000000Z",
                "validTo": "2020-12-31T23:59:59.999999Z"
            },
            {
                "price": 9.92,
                "validFrom": "2021-01-01T00:00:00.000000Z",
                "validTo": "2021-01-31T23:59:59.999999Z"
            },
            {
                "price": 9.98,
                "validFrom": "2021-02-01T00:00:00.000000Z",
                "validTo": "2021-02-28T23:59:59.999999Z"
            }
        ],
        "images": [
            "https:\/\/depot.dam.aboutyou.cloud\/images\/5fb50f3fb2676.jpg?width=1200&height=1200",
            "https:\/\/depot.dam.aboutyou.cloud\/images\/5fb50f3fb26b1.jpg?width=1200&height=1200",
            "https:\/\/depot.dam.aboutyou.cloud\/images\/5fb50f3fb26ef.jpg?width=1200&height=1200"
        ]
    }
```

## Outputs
#### External API
The application should export the product details to a certain api with the following specifications
* Method: POST
* Authorization: Bearer
* Body: see attached (external-api.json) file


#### Request Product details API
You need to design the api which accept the product identifier as input and return the product details (see attached product-details.json file)




## Setup project



- clone repo
- composer install
- ./vendor/bin/sail
- ./vendor/bin/sail php artisan migrate
- ./vendor/bin/sail php artisan storage:link
- cp Sample_data/sfpt-input-test-data.csv Storage/app



## Sample data

sfpt-input-test-data.csv  is in Sample_data folder need to be coped to Storage/app beacose application is setup to connects to free SFTP service for testing purpose. To connect to real add you credentials in config/filesystems.php all this block shud be added to .env file in future

## Postman collection

Postman collection is in Sample_data/aboutyou.postman_collection.json

Contains test and needed routes for application

